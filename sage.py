import pandas as pd
import numpy as np
duplicate_check=[]

file_loc = "SAGE Digital.xlsx"

df = pd.read_excel(file_loc,sheet_name="SAGE Invoice_SRMDRO",skiprows=8,skipfooter=1)
srmdro = df['Invoice #']

master = pd.read_excel(file_loc,sheet_name="SAGE Invoice_SRMDRO_ATLD",skiprows=7,skipfooter=1)
df2 = master['Invoice #']

df['Duplicate_check'] = np.where(df['Invoice #'].equals (master['Invoice #']),'found', 'Not found')
df1 = df.rename(columns=lambda x: '' if x.startswith('Unnamed') else x)

df1.to_csv("Duplicates.csv",index=False)

from google.colab import files
files.download("Duplicates.csv")